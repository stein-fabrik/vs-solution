#pragma once
#include <wx/wx.h>
#include <wx/dc.h>
#include <wx/event.h>

#define uint unsigned int
#define ulong unsigned long


#define TD_ALIGN_LEFT 0u
#define TD_ALIGN_CENTER 1u
#define TD_ALIGN_RIGHT 2u

#define POINT_TAG_TOP  0u
#define POINT_TAG_RIGHT 1u
#define POINT_TAG_BOTTOM 2u
#define POINT_TAG_LEFT 3u

using std::to_string;

//DEFAULT INCLUDE