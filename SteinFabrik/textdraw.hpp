#pragma once
#include "stdinc.hpp"

struct textdraw //CUSTOM DRAWABLE TEXT
{
	public:
		textdraw() {};
		textdraw(wxString text, wxPoint position, 
			wxFont font = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL), 
			wxColour colour = wxColour(0, 0, 0, 0xFF), uint align = TD_ALIGN_LEFT);
	public:
		wxString      text;
		wxPoint       position;
		wxFont        font;
		uint          align;
		wxColour      colour;
		virtual void  draw(wxDC& dc);  //TEXTDRAW DRAWING METHOD
};