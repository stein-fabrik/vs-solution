#include "textdraw.hpp"

textdraw::textdraw(wxString text, wxPoint position, wxFont font, wxColour colour, uint align)
{
	this->text = text;
	this->position = position;
	this->font = font;
	this->colour = colour;
	this->align = align;
}

void textdraw::draw(wxDC& dc)
{
	dc.SetFont(this->font);
	dc.SetTextForeground(this->colour);
	this->position.x -= this->align * floor(dc.GetTextExtent(this->text).x / 2.0); // ALIGNMENT MECHANISM
	dc.DrawText(this->text, this->position);
};