#include "line.hpp"

line::line(point* start, point* end, wxPen style)
{
	this->start = start;
	this->end = end;
	this->style = style;
}

void line::draw(wxDC& dc) // LINE DRAWING FUNCTION
{
	dc.SetPen(this->style);
	dc.DrawLine(this->start->coords, this->end->coords);
}