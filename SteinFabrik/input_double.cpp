#include "input_double.hpp"

input_double::input_double(wxWindow* parent, wxWindowID id, wxString text, wxString name, wxString metric, wxPoint pos, wxSize size, wxFont font, wxColour colour)
	: wxSpinCtrlDouble(parent, id, text, pos, size, wxSP_WRAP, 0, 999999, 0, 0.5)
{
	this->name = new textdraw(name, wxPoint(pos.x - 10, pos.y + 2), font, colour, TD_ALIGN_RIGHT); //SOME MAGIC TRICKS WITH POSITIONING
	this->metric = new textdraw(metric, wxPoint(pos.x + size.x + 5, pos.y + 4), font, colour, TD_ALIGN_LEFT); //SOME MORE MAGIC TRICKS
};

void input_double::draw(wxDC& dc)
{
	//RESET ON EVERY PAINT EVENT OCCURANCE
	this->name->position = this->GetPosition();
	this->name->position.x -= 10;
	this->name->position.y += 2;

	this->metric->position = this->GetPosition();
	this->metric->position.x += this->GetSize().x + 5;
	this->metric->position.y += 4;
	//RESET ON EVERY PAINT EVENT OCCURANCE

	this->name->draw(dc);
	
	this->metric->draw(dc);
}