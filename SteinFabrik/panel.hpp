#include "stdinc.hpp"

struct panel //CUSTOMIZABLE PANEL
{
	public:
		panel() {};
		panel(wxPoint position, wxSize size, wxColour colour = wxColour(0xFF, 0xFF, 0xFF, 0xFF), wxPen outline = wxPen(wxColor(0, 0, 0, 0xFF), 3, wxPENSTYLE_SOLID), uint frame_rounding = 8u);
	public:
		wxPoint        position;
		wxSize         size;
		wxColour       colour;
		wxPen          outline;
		uint           frame_rounding;
		virtual void   draw(wxDC &dc);
};