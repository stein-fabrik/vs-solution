#include "point.hpp"

point::point(wxString name, wxPoint coords, uint style, wxFont font, wxColour colour, wxPen outline, uint radius)
{
	this->coords = coords;
	this->colour = colour;
	this->outline = outline;
	this->radius = radius;
	this->style = style;
	switch (style)
	{
		case POINT_TAG_TOP:
			style = TD_ALIGN_CENTER;
			coords.y -= (radius + outline.GetWidth() + font.GetPixelSize().y);
			break;
		case POINT_TAG_RIGHT:
			style = TD_ALIGN_LEFT;
			coords.x += (font.GetPixelSize().x * 2 + radius + outline.GetWidth());
			coords.y -= font.GetPixelSize().y / 2;
			break;
		case POINT_TAG_BOTTOM:
			style = TD_ALIGN_CENTER;
			coords.y += (radius + outline.GetWidth());
			break;
		case POINT_TAG_LEFT:
			style = TD_ALIGN_RIGHT;
			coords.x -= (font.GetPixelSize().x * 2 + radius + outline.GetWidth());
			coords.y -= font.GetPixelSize().y / 2;
			break;
	}
	//TEXTDRAW POSITIONING DEPENDING ON POINT DRAW STYLE
	this->name = new textdraw(name, coords, font, outline.GetColour(), style);
};

void point::draw(wxDC &dc)
{
	dc.SetBrush(wxBrush(this->colour));
	dc.SetPen(this->outline);
	dc.DrawCircle(this->coords, radius);
	//RESET ON EVERY EVENT OCCURANCE
	this->name->position = this->coords;
	switch (this->style)
	{
		case POINT_TAG_TOP:
			this->name->align = TD_ALIGN_CENTER;
			this->name->position.y -= (radius + outline.GetWidth() + this->name->font.GetPixelSize().y);
			break;
		case POINT_TAG_RIGHT:
			this->name->align = TD_ALIGN_LEFT;
			this->name->position.x += (this->name->font.GetPixelSize().x * 2 + radius + outline.GetWidth());
			this->name->position.y -= this->name->font.GetPixelSize().y / 2;
			break;
		case POINT_TAG_BOTTOM:
			this->name->align = TD_ALIGN_CENTER;
			this->name->position.y += (radius + outline.GetWidth());
			break;
		case POINT_TAG_LEFT:
			this->name->align = TD_ALIGN_RIGHT;
			this->name->position.x -= (this->name->font.GetPixelSize().x * 2 + radius + outline.GetWidth());
			this->name->position.y -= this->name->font.GetPixelSize().y / 2;
			break;
	}
	//RESET ON EVERY EVENT OCCURANCE
	this->name->draw(dc);
}