#include "math_stuff.hpp"

/*ALEX MALEYEV PRODUCTION*/

static inline double get_extremum(double h, double t1, double t2)
{
    double m = t1 / t2;
    if (islessequal(m, 1)) return -1;
    else return h / sqrt(m * m - 1);
}

static inline double price(double x, double L, double h, double t1, double t2) { return t1 * sqrt(h * h + x * x) + t2 * (L - x); }

output_container get_output(double h, double L, double t1, double t2)
{
    double x0 = get_extremum(h, t1, t2);

    output_container sol;
    double min_cost;

    double C_L = price(L, L, h, t1, t2);
    double C_0 = price(0, L, h, t1, t2);

    if (isless(x0, 0) || isgreater(x0, L))
    {
        min_cost = min(C_L, C_0);
        if (min_cost == C_0)
        {
            sol.cost = C_0;
            sol.distance = 0;
            return sol;
        }
        else
        {
            sol.cost = C_L;
            sol.distance = L;
            return sol;
        }
    }
    else
    {
        double C_x = price(x0, L, h, t1, t2);
        min_cost = min(min(C_0, C_L), C_x);

        if (min_cost == C_0)
        {
            sol.cost = C_0;
            sol.distance = 0;
            return sol;
        }
        else if (min_cost == C_x)
        {
            sol.cost = C_x;
            sol.distance = x0;
            return sol;
        }
        else
        {
            sol.cost = C_L;
            sol.distance = L;
            return sol;
        }
    }
}