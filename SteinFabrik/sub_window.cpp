#include "sub_window.hpp"

sub_window::sub_window(wxString header, wxPoint position, wxSize size, wxFont font, wxColour colour, wxColour font_colour, wxPen outline, uint frame_rounding)
{ 
	this->main = new panel(position, size, colour, outline, frame_rounding);
	this->header = new textdraw(header, wxPoint(position.x + size.x / 2, position.y + font.GetPointSize() / 2), font, font_colour, TD_ALIGN_CENTER);
};

void sub_window::draw(wxDC& dc)
{
	this->header->position = wxPoint(main->position.x + main->size.x / 2, main->position.y + header->font.GetPointSize() / 2); //RESET ON EVERY EVENT OCCURANCE
	main->draw(dc);
	header->draw(dc);
};