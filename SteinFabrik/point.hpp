#include "textdraw.hpp"

struct point //DRAWABLE POINT
{
	public:
		point() {};
		point(wxString name, wxPoint coords, uint style, wxFont font = wxFont(14, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD),
			wxColour colour = wxColour(0xFF, 0xFF, 0xFF, 0xFF), 
			wxPen outline = wxPen(wxColour(0, 0, 0, 0xFF), 4, wxPENSTYLE_SOLID), uint radius = 8);
	public:
		textdraw*  name = nullptr;
		wxPoint    coords;
		wxColour   colour;
		wxPen      outline;
		uint       style; //POINT NAME DISPLAY TYPE
		uint       radius;
		void       draw(wxDC &dc);
};