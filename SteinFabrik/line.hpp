#include <wx/statline.h>
#include "point.hpp"

struct line // DRAWABLE LINE BETWEEN TWO POINTS
{
	public:
		line() {};
		line(point* start, point* end, wxPen style = wxPen(wxColour(0, 0, 0, 0xFF), 6, wxPENSTYLE_SOLID));
	public:
		point*  start = nullptr;
		point*  end = nullptr;
		wxPen   style;
		void    draw(wxDC& dc);
};