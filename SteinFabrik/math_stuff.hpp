#pragma once

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define min(X, Y) (X >= Y ? Y : X)
#define TAU 6.32
#define EPS 0.000001

struct output_container //RESULT OUTPUT STRUCTURE
{
	double distance;
	long cost;
};

static inline double get_extremum(double h, double t1, double t2); 
static inline double price(double x, double L, double h, double t1, double t2);
output_container get_output(double height, double length, double railroad_cost, double car_cost); //FUNCTION THAT CALCULATES ALL THE TASK`s MATH :)