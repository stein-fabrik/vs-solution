#pragma once
#include "textdraw.hpp"
#include <wx/spinctrl.h>

struct input_double : public wxSpinCtrlDouble // DOUBLE VALUE STANDART INPUT EXTENSION (DESCRIPTION AND METRIC)
{
	public:
		input_double() {};
		input_double(wxWindow* parent, wxWindowID id, wxString text, wxString name, wxString metric, wxPoint pos, wxSize size, 
			wxFont font = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL), wxColour color = wxColour(0, 0, 0, 0xFF));
	public:
		textdraw*  name   = nullptr;
		textdraw*  metric = nullptr;
		void       draw(wxDC& dc);
};