#include "main.hpp"

wxIMPLEMENT_APP(main);

main::main() {};
main::~main() {};

bool main::OnInit() // APP INIT 
{
	main_window = new window("SteinFabrik");
	main_window->SetIcon(wxIcon(wxIconLocation(wxGetCwd() + "\\icon.ico")));
	main_window->SetBackgroundColour(wxColour(0xFF, 0xFF, 0xFF, 0xFF));
	main_window->Show();
	return true;
};