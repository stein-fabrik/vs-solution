#include "textdraw.hpp"
#include "panel.hpp"

struct sub_window //PANEL WITH HEADER
{
	public:
		sub_window() {};
		sub_window(wxString header, wxPoint position, wxSize size, 
			wxFont font = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL), 
			wxColour colour = wxColour(0xFF, 0xFF, 0xFF, 0xFF), wxColour font_colour = wxColour(0, 0, 0, 0xFF), 
			wxPen outline = wxPen(wxColor(0, 0, 0, 0xFF), 3, wxPENSTYLE_SOLID), uint frame_rounding = 8u);
	public: 
		panel*     main;
		textdraw*  header;
		void       draw(wxDC& dc); 
};