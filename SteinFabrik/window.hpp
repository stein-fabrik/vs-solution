#include "sub_window.hpp"
#include "line.hpp"
#include "input_double.hpp"
#include "math_stuff.hpp"

#define MAX_RAILROAD_LENGTH 700.0
#define MAX_QUARRY_DISTANCE 300.0


struct window : wxFrame //ALL UI COMPONENTS CONTAINER
{
	public:
		window();
		window(const char* name);
		~window();
	private:        
		//LEGEND
		panel          legend_box; 
		textdraw       legend_text;
		//LEGEND

		sub_window*    map             = nullptr; //MATH MODEL
		sub_window*    range_box       = nullptr; //POSITIONING INPUT PANEL
		sub_window*    price_box       = nullptr; //PRICE INPUT PANEL
		sub_window*    result_box      = nullptr; //RESULT TEXT OUTPUT PANEL

		textdraw*      output_dist_f   = nullptr; //TEXT OUTPUT FOR "SF" SEGMENT DISTANCE
		textdraw*      output_dist_h   = nullptr; //TEXT OUTPUT FOR "HS" SEGMENT DISTANCE
		textdraw*      output_angle    = nullptr; //TEXT OUTPUT FOR "HQS" ANGLE
		textdraw*      output_cost     = nullptr; //TEXT OUTPUT FOR TRANSPORTATION COST

		point*         H               = nullptr; //POINT H RENDERER
		point*         Q               = nullptr; //POINT Q RENDERER
		point*         F               = nullptr; //POINT F RENDERER
		point*         S               = nullptr; //POINT S RENDERER

		line*          railway	       = nullptr; //RAILWAY (HF) LINE RENDERER
		line*          road            = nullptr; //ROAD (QH) LINE RENDERER
		line*          path            = nullptr; //RAILWAY (QS) LINE RENDERER

		wxPanel*       empty           = nullptr; //NEVERMIND
		input_double*  factory         = nullptr; //DISTANCE BETWEEN FACTORY (F) AND PROJECTION (H)
		input_double*  quarry          = nullptr; //DISTANCE BETWEEN QUARRY (Q) AND THE RAILROAD
		input_double*  car_cost        = nullptr; //CAR TRANSPORTATION TARIF
		input_double*  railroad_cost   = nullptr; //RAILROAD TRANSPORTATION TARIF


		void    calculate_output();                        
		//CALCULATES THE OUTPUT 
		//1. DISTANCE BETWEEN THE QUARRY PROJECTION AND THE STATION
		//2. DISTANCE BETWWEEN THE STATION AND THE FACTORY
		//3. THE ANGLE HQS (PROJECTION, QUARRY, STATION)
		//4. TRANSPORTATION COST
		void    on_paint(wxPaintEvent& event);             //DRAW OBJECTS EVENT
		void    on_change_value(wxSpinDoubleEvent& event); //INPUT VALUE CHANGE EVENT

		wxDECLARE_EVENT_TABLE();
};