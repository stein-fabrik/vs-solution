﻿#include "window.hpp"

wxBEGIN_EVENT_TABLE(window, wxFrame)
	EVT_PAINT(on_paint)
	EVT_SPINCTRLDOUBLE(0, on_change_value)
	EVT_SPINCTRLDOUBLE(1, on_change_value)
	EVT_SPINCTRLDOUBLE(2, on_change_value)
	EVT_SPINCTRLDOUBLE(3, on_change_value)
wxEND_EVENT_TABLE()

window::window() {};

window::~window() {};

window::window(const char* name) : wxFrame(nullptr, wxID_ANY, name, wxPoint(300, 300), wxSize(800, 600), wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN)
{
	this->empty = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(0, 0)); //NEVERMIND
	wxFont title = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD); //DEFAULT

	//PANELS INIT
	this->map = new sub_window("Креслення", wxPoint(5, 5), wxSize(774, 420), title);
	this->range_box = new sub_window("Розташування", wxPoint(5, 430), wxSize(200, 125), title);

	this->price_box = new sub_window("Тарифи за 1 км", wxPoint(210, 430), wxSize(200, 125), title);
	this->result_box = new sub_window("Результати обчислень", wxPoint(415, 430), wxSize(365, 125), title);
	//PANELS INIT

	//LEGEND INIT
	this->legend_box = panel(wxPoint(470, 315), wxSize(300, 100), wxColour(0xFF, 0xFF, 0xFF, 0xFF), wxPen(wxColor(0x32, 0x32, 0x32, 0xFF), 3, wxPENSTYLE_SOLID));
	const char text[256] = "F - розташування заводу\nQ - розташувенная кар'єру\nH - точка на залізниці ближча до кар'єру\nS - оптимальне місце для побудування\n залізничної станції";
	this->legend_text = textdraw(text, wxPoint(484, 324));
	//LEGEND INIT

	//MATH MODEL 
	this->H = new point("H", wxPoint(40, 60), POINT_TAG_LEFT);
	this->Q = new point("Q", wxPoint(40, 360), POINT_TAG_BOTTOM);
	this->F = new point("F", wxPoint(740, 60), POINT_TAG_RIGHT);

	this->railway = new line(H, F);
	this->road = new line(Q, H, wxPen(wxColour(0, 0, 0, 0xFF), 6, wxPENSTYLE_DOT));
	//MATH MODEL

	//INPUT VALUES
	this->factory = new input_double(this, 0, "700", "HF", "км", wxPoint(56, 465), wxSize(100, 20));
	this->quarry = new input_double(this, 1, "300", "HQ", "км", wxPoint(56, 505), wxSize(100, 20));
	this->car_cost = new input_double(this, 2, "1000", "автомобілем", "грн", wxPoint(300, 465), wxSize(80, 20), wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	this->railroad_cost = new input_double(this, 3, "100", "залізницею  ", "грн", wxPoint(300, 505), wxSize(80, 20));
	//INPUT VALUES

	//TEXT OUTPUT
	this->output_dist_f = new textdraw("SF:            XXX км", wxPoint(524, 457));
	this->output_dist_h = new textdraw("HS:            XXX км", wxPoint(524, 477));
	this->output_angle = new textdraw("Кут HQS:    XX°", wxPoint(524, 497));
	this->output_cost = new textdraw("Ціна:          XXXX грн", wxPoint(524, 517));
	//TEXT OUTPUT
	this->calculate_output();
};

void window::on_paint(wxPaintEvent& event)
{
	//PANELS
	wxPaintDC dc(this);

	this->map->draw(dc);
	this->range_box->draw(dc);
	this->price_box->draw(dc);
	this->result_box->draw(dc);

	this->factory->draw(dc);
	this->quarry->draw(dc);
	this->car_cost->draw(dc);
	this->railroad_cost->draw(dc);
	//PANELS

	//LEGEND
	this->legend_box.draw(dc);
	this->legend_text.draw(dc);
	//LEGEND

	//MATH MODEL
	this->railway->draw(dc);
	this->road->draw(dc);
	if (this->path != nullptr) this->path->draw(dc);

	this->H->draw(dc);
	this->F->draw(dc);
	this->Q->draw(dc);
	if (this->S != nullptr) this->S->draw(dc);
	//MATH MODEL

	//CALCULATION RESULT OUTPUT
	this->output_dist_f->draw(dc);
	this->output_dist_h->draw(dc);
	this->output_angle->draw(dc);
	this->output_cost->draw(dc);
	//CALCULATION RESULT OUTPUT
};

void window::on_change_value(wxSpinDoubleEvent& event)
{
	this->calculate_output(); //CHANGING THE MAP AND FILLING TEXT FIELDS
	this->Refresh(false); //REFRESH SCREEN
};

void window::calculate_output()
{
	wxPoint h_coords = road->end->coords; // A LIL SHORTCUT
	double height_len = this->quarry->GetValue(); // AND ANOTHER ONE
	double railroad_len = this->factory->GetValue(); // AND ANOTHER ONE

	double relation = 1;
	if (height_len <= EPS || railroad_len <= EPS) //HANDLING 0 DIVISION CASES
	{
		road->start->coords.y = height_len <= EPS ? h_coords.y : h_coords.y + MAX_QUARRY_DISTANCE;
		railway->end->coords.x = railroad_len <= EPS ? h_coords.x : h_coords.x + MAX_RAILROAD_LENGTH;
		relation = height_len <= EPS ? railroad_len / EPS : railroad_len / height_len;
	}
	else //GENERAL CASE
	{
		double relation = railroad_len / height_len; //RELATION OF RAILROAD AND HEIGHT LENGTHS
		double etalon = MAX_RAILROAD_LENGTH / MAX_QUARRY_DISTANCE; // 2.3(3)

		if (relation >= etalon) // => HEIGHT DECREASES
		{
			road->start->coords.y = h_coords.y + 1 / relation * MAX_RAILROAD_LENGTH;
			railway->end->coords.x = h_coords.x + MAX_RAILROAD_LENGTH;
		}
		else // => RAILROAD LENGTH DECREASES
		{
			railway->end->coords.x = h_coords.x + relation * MAX_QUARRY_DISTANCE;
			road->start->coords.y = h_coords.y + MAX_QUARRY_DISTANCE;
		}
	}
	output_container result = get_output(height_len, railroad_len, this->car_cost->GetValue(), this->railroad_cost->GetValue());

	//CALCULATED PATH INIT
	int offset_x = railroad_len <= EPS ? 0 : result.distance / railroad_len * (this->F->coords.x - h_coords.x); //ANOTHER ZERO DIVISION CASE HANDLING
	this->S = new point("S", wxPoint(h_coords.x + offset_x, h_coords.y), POINT_TAG_TOP);
	this->path = new line(this->Q, this->S);
	//CALCULATED PATH INIT

	this->output_dist_f->text = wxString::Format("SF:            %s км", to_string(railroad_len - result.distance));
	this->output_dist_h->text = wxString::Format("HS:            %s км", to_string(result.distance));

	this->output_angle->text = wxString::Format("Кут HQS:    %s°", to_string(height_len <= EPS ? 0 :(long)floor(atan(result.distance / height_len) * 360 / TAU)));
	
	this->output_cost->text = wxString::Format("Ціна:          %s грн", to_string(result.cost));
};