#pragma once
#include "window.hpp"

struct main : public wxApp //APPLICATION ENTRY POINT
{
	public:
		main();
		~main();
	public:
		window* main_window = nullptr;
		virtual bool OnInit();
};