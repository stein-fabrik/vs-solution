#include "panel.hpp"

panel::panel(wxPoint position, wxSize size, wxColour colour, wxPen outline, uint frame_rounding)
{
    this->position = position;
    this->size = size;
    this->colour = colour;
    this->outline = outline;
    this->frame_rounding = frame_rounding;
};

void panel::draw(wxDC& dc)
{
    dc.SetPen(this->outline);
    dc.SetBrush(this->colour);

    wxRect rect(this->position, this->size);
    dc.DrawRoundedRectangle(rect, this->frame_rounding);
};